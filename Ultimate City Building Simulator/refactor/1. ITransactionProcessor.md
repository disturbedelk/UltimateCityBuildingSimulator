﻿**Так как это первый коммит - краткое пояснение ситуации:**
Код написан со мной в кооперации в рамках одного из ООП предметов на каком-то из прошлых курсов и писался с квотой на использование интерфейсов и каких-то прочих принципов ООП, из-за чего сами эти принципы зачастую нарушались. Это примерно как с квотой на рефакторинг в 8 баллов, когда надо исправить все то, что до этого в исправлении не нуждалось.  


#### Нарушение принципа единственной ответственности и стрельба дробью

Реализация ITransactionProcessor, призванная выполнять операции на основании транзакций, в PerformTransaction занимается паттерн матчингом транзакций для выполнения простых, но разных модификаций на себе. При этом все модификации определяются отдельными наследниками команды, однако фактически несут в себе только тип и одно значение (value), в то время как логика выполнения операции находится вообще в самом TransactionProcessor. Получается мы разделили ответственность за выполнение операции на иерархию операций, которые являются болванками с данными (ОдНиМ дАнНыМ) и processor, который фактически их выполняет, причем в рамках одного метода, со сравнением классов и дублированием кода. Весь этот цирк призвано исправить следующее решение: Логика выполнения транзакций будет делегирована, внезапно - самим транзакциям в рамках шаблона "команда". Тогда сама транзакция вместо класса с данными (...) будет реализовывать конкретный тип транзакции и определять собственное состояние успеха операции.

<details>
<summary>

</summary>
```csharp
public void PerformTransaction(Transaction transaction, out TransactionState state, out int value)
        {
            value = -1;
            state = TransactionState.INTERNAL_ERROR;
            if (!TerminalsPermissions.ContainsKey(transaction.TerminalGuid))
            {
                value = -1;
                state = TransactionState.INTERNAL_ERROR;
                return;
            }
            switch (transaction)
            {
                case SetBalanceTransaction:
                    if ((TerminalsPermissions[transaction.TerminalGuid] & (int)Permissions.SET) != 0)
                    {
                        balance = transaction.Value;
                        state = TransactionState.OPERATION_SUCCESSFUL;
                    }
                    else
                    {
                        state = TransactionState.ACCESS_DENIED;
                    }
                    break;
                case AlterBalanceTransaction:
                    if ((TerminalsPermissions[transaction.TerminalGuid] & (int)Permissions.ALTER) != 0)
                    {
                        if (balance + transaction.Value >= 0)
                        {
                            balance += transaction.Value;
                            state = TransactionState.OPERATION_SUCCESSFUL;
                        }
                        else
                        {
                            state = TransactionState.INTERNAL_ERROR;
                        }
                    }
                    else
                    {
                        state = TransactionState.ACCESS_DENIED;
                    }
                    break;
                case GetBalanceTransaction:
                    if ((TerminalsPermissions[transaction.TerminalGuid] & (int)Permissions.GET) != 0)
                    {
                        value = balance;
                        state = TransactionState.OPERATION_SUCCESSFUL;
                    }
                    else
                    {
                        state = TransactionState.ACCESS_DENIED;
                    }
                    break;
                case EndSessionTransaction:
                    TerminalsPermissions.Remove(transaction.TerminalGuid);
                    state = TransactionState.OPERATION_SUCCESSFUL;
                    break;
            }
            return;
        }
```
</details>



2. 
- Условное *перемещение метода* и применение шаблона "команда". 
- (читать выше); Перемещение метода с целью избавиться от нарушения ответственности TransactionProcessor'ом и бредовой логики метода PerformTransaction (собственно перенести ответственность за транзакции обратно в классы транзакций), шаблон "команда" - подходящее архитектурное решение в рамках транзакций.
3. `981dcf18`
- В первую очередь повысилась читаемость - получилось избавиться от большого, очень некрасивого, запутанного и отвечающего за выполнение всех видов транзакций метода PerformTransaction, распределив ответственность между транзакциями. Также в рамках команд получилось использовать Guard Clause'ы и в целом иметь куда более понятную для использования архитектуру транзакций. В плане новых изменений - решилась проблема разделения ответсвтенности, теперь все новые транзакции будут сразу реализовывать требуемые операции, нам не придется лазить по другим классам и добавлять новые кейсы в длинный switch паттерн матчинга. При расширении функционала самого процессора, однако, придется (и пришлось) требуемые для взаимодействия вещи пробрасывать в интерфейс (как, например, CheckPermission), но об этом дальше.