﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimateCityBuildingSimulator.Game.Building;
using UltimateCityBuildingSimulator.Game.Building.Commercial;
using UltimateCityBuildingSimulator.Game.Building.Institutional;
using UltimateCityBuildingSimulator.Game.Building.Residential;

namespace UltimateCityBuildingSimulator.Game
{
    public class Showcase
    {
        public ReadOnlyCollection<IBuildable> BuildingsShowcase;
        public Showcase()
        {
            BuildingsShowcase = new List<IBuildable> {
                new Shop(),
                new Factory(),
                new Office(),
                new Hospital(),
                new School(),
                new PoliceStation(),
                new SmallHouse(),
                new MediumHouse(),
                new LargeHouse(),
            }.AsReadOnly();
        }
    }
}
