﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimateCityBuildingSimulator.Game.TransactionTypes;

namespace UltimateCityBuildingSimulator.Game
{
    public class TransactionProcessor : ITransactionProcessor
    {
        public int Balance { get; set; }

        private Dictionary<Guid, int> TerminalsPermissions;

        public TransactionProcessor()
        {
            Balance = 30;
            TerminalsPermissions = new Dictionary<Guid, int>();
        }

        public bool CheckPermission(Guid terminal, Permissions perm)
        {
            return (TerminalsPermissions[terminal] & (int)perm) != 0;
        }

        public void EndSession(Guid terminal)
        {
            TerminalsPermissions.Remove(terminal);
        }

        public void PerformTransaction(Transaction transaction)
        {
            if (!TerminalsPermissions.ContainsKey(transaction.Terminal.Guid))
            {
                transaction.Invalidate(TransactionState.INTERNAL_ERROR);
                return;
            }

            foreach(var perm in transaction.RequiredPerms)
            {
                if(!CheckPermission(transaction.Terminal.Guid, perm))
                {
                    transaction.Invalidate(TransactionState.ACCESS_DENIED);
                    return;
                }
            }
            
            transaction.Execute(this);
        }

        public ITransactionProcessorTerminal GetTransactionProcessorTerminal()
        {
            TransactionProcessorTerminal NewTransactionProcessorTerminal = new TransactionProcessorTerminal(this, Guid.NewGuid());
            TerminalsPermissions.Add(NewTransactionProcessorTerminal.Guid, (int)(Permissions.GET | Permissions.SET | Permissions.ALTER));
            return NewTransactionProcessorTerminal;
        }


        public enum Permissions
        {
            GET = 1,
            SET = 2,
            ALTER = 4
        }
        public enum TransactionState
        {
            OPERATION_SUCCESSFUL = 1,
            INTERNAL_ERROR = -1,
            ACCESS_DENIED = 2
        }
    }
}
