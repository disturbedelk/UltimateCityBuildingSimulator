﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimateCityBuildingSimulator.Game.TransactionTypes;
using static UltimateCityBuildingSimulator.Game.TransactionProcessor;

namespace UltimateCityBuildingSimulator.Game
{
    public interface ITransactionProcessorTerminal
    {
        Guid Guid { get; }
        bool Process<T>(T transaction, out int result) where T : Transaction;
    }
}
