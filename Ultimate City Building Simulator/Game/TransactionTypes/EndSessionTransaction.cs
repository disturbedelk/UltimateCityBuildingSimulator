﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static UltimateCityBuildingSimulator.Game.TransactionProcessor;

namespace UltimateCityBuildingSimulator.Game.TransactionTypes
{
    internal class EndSessionTransaction : Transaction
    {
        public EndSessionTransaction(ITransactionProcessorTerminal term, int value = 0) : base(term, value) { }

        public override bool Execute(ITransactionProcessor processor)
        {
            processor.EndSession(Terminal.Guid);
            State = TransactionState.OPERATION_SUCCESSFUL;
            return true;
        }
    }
}
