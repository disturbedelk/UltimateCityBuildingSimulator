﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static UltimateCityBuildingSimulator.Game.TransactionProcessor;

namespace UltimateCityBuildingSimulator.Game.TransactionTypes
{
    internal class AlterBalanceTransaction : Transaction
    {
        public AlterBalanceTransaction(ITransactionProcessorTerminal term, int value = 0) : base(term, value) {
            RequiredPerms = new List<Permissions>{ 
                Permissions.ALTER 
            };
        }        

        public override bool Execute(ITransactionProcessor processor)
        {
            if (processor.Balance + Value >= 0)
            {
                processor.Balance += Value;
                State = TransactionState.OPERATION_SUCCESSFUL;
            }
            else
            {
                State = TransactionState.INTERNAL_ERROR;
                return false;
            }

            return true;
        }
    }
}
