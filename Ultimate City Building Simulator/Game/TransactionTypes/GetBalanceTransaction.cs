﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static UltimateCityBuildingSimulator.Game.TransactionProcessor;

namespace UltimateCityBuildingSimulator.Game.TransactionTypes
{
    internal class GetBalanceTransaction : Transaction
    {
        public GetBalanceTransaction(ITransactionProcessorTerminal term, int value = 0) : base(term, value) {
            RequiredPerms = new List<Permissions> { Permissions.GET };
        }

        public override bool Execute(ITransactionProcessor processor)
        {
            Result = processor.Balance;
            State = TransactionState.OPERATION_SUCCESSFUL;
            return true;
        }
    }
}
