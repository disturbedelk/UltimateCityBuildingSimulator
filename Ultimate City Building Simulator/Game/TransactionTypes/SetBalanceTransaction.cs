﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static UltimateCityBuildingSimulator.Game.TransactionProcessor;

namespace UltimateCityBuildingSimulator.Game.TransactionTypes
{
    internal class SetBalanceTransaction: Transaction
    {
        public SetBalanceTransaction(ITransactionProcessorTerminal term, int value = 0) : base(term, value) {
            RequiredPerms = new List<Permissions> { Permissions.SET };
        }

        public override bool Execute(ITransactionProcessor processor)
        {
            processor.Balance = Value;
            State = TransactionState.OPERATION_SUCCESSFUL;
            return true;
        }
    }
}
