﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static UltimateCityBuildingSimulator.Game.TransactionProcessor;

namespace UltimateCityBuildingSimulator.Game.TransactionTypes
{
    public abstract class Transaction
    {
        public int Value { get; protected set; }
        public ITransactionProcessorTerminal Terminal { get; private set; }
        public IReadOnlyCollection<Permissions> RequiredPerms { get; protected set; } = new List<Permissions>();
        public TransactionState State { get; protected set; } = TransactionState.INTERNAL_ERROR;
        public int Result { get; protected set; } = -1;

        public virtual void Invalidate(TransactionState state)
        {
            Result = -1;
            State = state;
        }

        public Transaction(ITransactionProcessorTerminal terminal, int value = 0)
        {
            Terminal = terminal;
            Value = value;
        }

        public abstract bool Execute(ITransactionProcessor processor);
    }
}
