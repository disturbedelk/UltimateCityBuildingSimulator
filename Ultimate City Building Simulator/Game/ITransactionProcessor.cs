﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimateCityBuildingSimulator.Game;
using UltimateCityBuildingSimulator.Game.TransactionTypes;
using static UltimateCityBuildingSimulator.Game.TransactionProcessor;

namespace UltimateCityBuildingSimulator.Game
{
    public interface ITransactionProcessor
    {
        int Balance { get; set; }

        void PerformTransaction(Transaction transaction);
        ITransactionProcessorTerminal GetTransactionProcessorTerminal();
        void EndSession(Guid terminal);
    }
}
