﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimateCityBuildingSimulator.Game.TransactionTypes;
using static UltimateCityBuildingSimulator.Game.TransactionProcessor;

namespace UltimateCityBuildingSimulator.Game
{
    public class TransactionProcessorTerminal : ITransactionProcessorTerminal
    {
        private ITransactionProcessor parentalProcessor;
        public Guid Guid { get; private set; }

        public bool Process<T>(T transaction, out int result) where T : Transaction
        {
            parentalProcessor.PerformTransaction(transaction);
            result = transaction.Result;
            return transaction.State == TransactionState.OPERATION_SUCCESSFUL;
        }

        public TransactionProcessorTerminal(ITransactionProcessor parentalProcessor, Guid guid)
        {
            this.parentalProcessor = parentalProcessor;
            this.Guid = guid;
        }
    }
}
